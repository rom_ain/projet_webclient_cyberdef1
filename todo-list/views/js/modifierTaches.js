/**
 * La fonction "ajouter(nomTache)" permet d'ajouter une tâche à la todoList. Cette fonction est appelée lorsque l'on appuit sur le boutton "Ajouter"
 * Dans cette fonction, on vérifie d'abord que la chaine entrée n'est pas vide, 
 * puis on vérifie que la tâche n'existe pas déjà 
 * et si toutes ces conditions sont réunies alors on ajoute la nouvelle tâche en l'initialisant à "A_FAIRE".
 * Enfin, on rafraichit la page.
 * @param { string } nomTache
 */
function ajouter(nomTache){
    console.log("ajouter");
    //on vérifie que le paramètre n'est pas vide
    if (nomTache == ""){
        alert("Le nom de la tâche ne peux pas être vide!");
        return 0;
    }

    //on fait une requête GET à notre API REST pour savoir si une tâche du même nom existe déjà
    fetch('/taches/'+nomTache ,{
        headers:{
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        },
        method: 'GET'
    }).then(reponse => reponse.json())
    .then(json => {
        if(json.message.length != 0){
            alert("La tâche '"+nomTache+"' existe déjà... Crées en une autre!");
        }else{// Si aucune tâche du même nom existe, alors on peu la créer en faisant une requête POST à notre API REST
            fetch('/taches',{
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({"libelle": nomTache})
            }).then( res=>{
                console.log(nomTache+" à été àjoutée à la TODO list");
                document.location.reload(true);
            })
            .catch(err => console.log(err));
        }
    });
};

/**
 * La fonction "supprimer(id)" permet de supprimer une tâche à la todoList. Cette fonction est appelée lorsque l'on appuit sur la croix rouge de la tache
 * Dans cette fonction, on supprime la tâche de la base de donnée et on raifraichit la page
 * @param { int } id
 */
function supprimer(id){
    //on fait une requête DELETE à notre API REST
    fetch('/taches', {
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'DELETE',
        body: JSON.stringify({"id": id})
    }).then(res=>{
        console.log(id+" à été supprimé de la TODO list");
        document.location.reload(true);
    })
    .catch(err => console.log(err));
    };

/**
 * La fonction "modifierStatus(status, id)" permet de modifier le status d'une tâche à la todoList. Cette fonction est appelée lorsque l'on appuit sur le boutton "DONE" ou "TODO", selon la tâche
 * Dans cette fonction, on regarde si le status de la tâche est à "A_FAIRE" ou à "FAIT", puis selon le résultat, on modifie la valeur.
 * @param { string } status
 * @param { int } id
 */
function modifierStatus(status, id){
    status.toString()
    //console.log(id+" : "+status);
    if(status == 'A_FAIRE'){
        var newStatus = 'FAIT';
    }else{
        var newStatus = 'A_FAIRE';
    }
    //on fait une requête PUT à notre API REST
    fetch('/taches',{
        headers:{
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        },
        method: 'PUT',
        body: JSON.stringify({'status': newStatus, 'id': id})
    }).then(res=>{
        console.log(id+" est passé de "+status+ " à "+newStatus);
        document.location.reload(true);
    })
    .catch(err => console.log(err));
};

/**
 * La fonction "modifierNomTache(id, newName)" permet de modifier le libelle d'une tâche à la todoList. 
 * Cette fonction est appelée par la fonction openForm, qui elle même est appelée lorsqu'on appuye sur le boutton "modifier" de la tache
 * Dans cette fonction, on récupère le nouveau nom de la tâche via un input, puis on fait une requete pour modifier la valeur de libelle du tuple dans la base de donnée
 * @param { int } id 
 * @param { string } newName
 */
async function modifierNomTache(id, newName){
    //on fait une requête GET à notre API REST pour savoir si une tâche du même nom existe déjà
    await fetch('/taches/'+newName ,{
        headers:{
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        },
        method: 'GET'
    }).then(reponse => reponse.json())
    .then(json => {
        if(json.message.length != 0){
            alert("La tâche '"+newName+"' existe déjà... Crées en une autre!");
        }else{
            // Si aucune tâche du même nom existe, alors on peu la modifier en faisant une requête PUT à notre API REST
            fetch('/taches/modif', {
                headers:{
                    'Accept': 'application/json',
                    'Content-Type' : 'application/json'
                },
                method: 'PUT',
                body: JSON.stringify({'libelle': newName, 'id': id})
            }).then(res => {
                console.log("le nouveau nom de la tâche "+id+" est: "+newName);
                document.location.reload(true);
            })
            .catch(err => console.log(err));
        }
        closeForm();
        document.getElementById("newTacheName").value="";
    });
};

/**
 * La fonction "rechercher(nomTache)" permet de rechercher et d'afficher une tâche de la todoList. 
 * Cette fonction est appelée lorsqu'on appuit sur le boutton "rechercher"
 * Dans cette fonction, on on vérifier que l'entrée n'est pas vide, puis on fait une requete pour obtenir les infos relatifs à cette tâche
 * et enfin, on ajoute du code html si un tuple a été trouvé, permettant l'affichage de la tâche.
 * @param { string } nomTache
 */
function rechercher(nomTache){
    console.log("rechercher");
    if (nomTache == ""){
        alert("Le nom de la tâche ne peux pas être vide!");
        return 0;
    }
    //on fait une requête GET à notre API REST pour savoir si la tâche recherchée existe
    fetch('/taches/'+nomTache ,{
        headers:{
            'Accept': 'application/json',
            'Content-Type' : 'application/json'
        },
        method: 'GET'
    }).then(result => result.json())
    .then(json =>{
        console.log(json);
        if(json.message.length == 0){
            alert("La tâche '"+nomTache+"' n'existe pas dans la base de donnée... Crées la!");
        }else{
            //Si la tâche existe alors on l'affiche
            status = "'"+json.message[0].status+"'";
            libelle = json.message[0].libelle;
            id = json.message[0].id;
            //on ajoute du code HTML dans la balise <ul id="resRecherche">
            if (status === "'A_FAIRE'"){
                document.getElementById('resRecherche').innerHTML= 
                '<li class="list-group-item list-group-item-warning">'
                    +libelle
                    +'<div class="btn-group custom01" role="group" aria-label="Basic mixed styles example">'
                    +'    <button onclick="modifierStatus('+status+', '+id+')" type="button" class="btn btn-success btn-sm">DONE</button>'
                    +'    <button onclick="openForm('+id+')" type="button" class="btn btn-warning btn-sm">Modifier</button>'
                    +'    <button onclick="supprimer('+id+')" type="button" class="btn btn-danger btn-sm">X</button>'
                    +'</div>'                            
                +'</li>';
            }else{
                document.getElementById('resRecherche').innerHTML= 
                '<li class="list-group-item list-group-item-success">'
                    +'<strike>'+libelle+'</strike>'
                    +'<div class="btn-group custom01" role="group" aria-label="Basic mixed styles example">'
                    +'    <button onclick="modifierStatus('+status+', '+id+')" type="button" class="btn btn-success btn-sm">DONE</button>'
                    +'    <button onclick="openForm('+id+')" type="button" class="btn btn-warning btn-sm">Modifier</button>'
                    +'    <button onclick="supprimer('+id+')" type="button" class="btn btn-danger btn-sm">X</button>'
                    +'</div>'                            
                +'</li>';
            }
            
        }
    })
    .catch(err => console.log(err));

};


/**
 * La fonction "openForm(id)" permet d'appeler la fonction "modifierNomTache(id, newName)" lorsqu'on souhaite modifier une tâche. Elle est appelée lorsqu'on appuit sur le boutton "Modifier" d'une tâche
 * Cette fonction affiche une fênetre pop-up qui permet à l'utilisateur de renseigner un nouveau nom pour la tâche
 * Puis, la fonction va réupérer la valeur de cet input pour l'envoyer à la fonction "modifierNomTache(id, newName)", seulement si l'input n'est pas vide
 * @param { int } id
 */
function openForm(id){
    //on fait apparaitre le pop-up
    document.getElementById("popupForm").style.display="block";
    //lors du click sur le boutton "Valider" du formulaire, on récupère le nouveau nom de la tâche et on l'envoie (avec l'id) à la fonction "modifierNomTache" 
    document.getElementById("valider").addEventListener('click', ()=>{
        var newName = document.getElementById("newTacheName");
        if(newName.value===""){
            alert("Le nom de la tâche ne peux pas être vide!"); 
        }else{
            modifierNomTache(id,newName.value);
        }
    })   
}

/**
 * La fonction "closeForm()" permet de fermer le formulaire de modification de nom d'une tâche
 * elle est appelée lorsque l'on appuit sur 'annuler' ou 'valider'
 */
function closeForm() {
    document.getElementById("popupForm").style.display="none";
};