const express = require("express");
const bodyParser = require("body-parser");
const db = require("./db.js");

const path = require('path');
const app = express();

// use : Utilisation des Middlewares Express
app.use(bodyParser.json()); // Parse les requêtes avec content-type: application/json
app.use(bodyParser.urlencoded({ extended: true })); // Parse les requêtes avec content-type: application/x-www-form-urlencoded


//-----------------------------ROUTES------------------------------

// route pour afficher la todolist
app.get("/", (req, res) => {
  db.then( pool => pool.query('SELECT * FROM taches'))
    .then( results => {
      if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
      res.status(201);
      res.render('index.ejs', {todolist: results});
  }).catch( err => {
    console.log(err);
    res.sendStatus(500);
  });
});

// route pour rechercher une tâche
app.get("/taches/:recherche", (req, res) => {
  db.then( pool => pool.query('SELECT * FROM taches WHERE libelle=?', [req.params.recherche]))
    .then( results => {
      if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
      res.status(201);
      res.location("/taches/");
      res.json({ message: results });
    }).catch( err => {
      console.log(err);
      res.sendStatus(500);
    });
});

// route pour ajouter une tache à la todolist
app.post("/taches/", (req, res) => {
	db.then( pool => pool.query('INSERT INTO taches(libelle,status) VALUES(?,"A_FAIRE")', [req.body.libelle])) //Requete SQL avec les paramètres de la requete API
  .then( results => {
    if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
    //Si tout se passe bien alors statusCode = 201, et on envoit la réponse en JSON
    res.status(201);
    res.location("/taches/");
    res.json({ message: results });
	}).catch( err => {
    console.log(err);
    res.sendStatus(500); // Si l'on a une erreur lors de la query alors erreur 505
  });
});

// route pour supprimer une tache de la todolist
app.delete("/taches/", (req, res) => {
	db.then( pool => pool.query('DELETE FROM taches WHERE id=?', [req.body.id])) //Requete SQL avec les paramètres de la requete API
  .then( results => {
    if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
    //Si tout se passe bien alors statusCode = 201, et on envoit la réponse en JSON
    res.status(201);;
    res.location("/taches/");
    res.json({ message: results });
	}).catch( err => {
    console.log(err);
    res.sendStatus(500); // Si l'on a une erreur lors de la query alors erreur 505
  });
});

//route pour modifier l'état d'une tache
app.put("/taches/", (req, res)=>{
  db.then( pool => pool.query('UPDATE taches SET status=? WHERE id=?', [req.body.status, req.body.id])) //Requete SQL avec les paramètres de la requete API
  .then( results => {
    if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
    //Si tout se passe bien alors statusCode = 201, et on envoit la réponse en JSON
    status = 201;
    res.location("taches");
    res.json({message: results});
  }).catch( err => {
    console.log(err);
    res.sendStatus(500); // Si l'on a une erreur lors de la query alors erreur 505
  });
});

//route pour modifier le nom d'une tache
app.put("/taches/modif", (req, res)=>{
  db.then( pool => pool.query('UPDATE taches SET libelle=? WHERE id=?', [req.body.libelle, req.body.id])) //Requete SQL avec les paramètres de la requete API
  .then( results => {
    if(results.length === 0)res.statusCode = 404; // Si l'on a pas trouvé la ressource, alors erreur 404
    //Si tout se passe bien alors statusCode = 201, et on envoit la réponse en JSON
    status = 201;
    res.location("taches");
    res.json({message: results});
  }).catch( err => {
    console.log(err);
    res.sendStatus(500); // Si l'on a une erreur lors de la query alors erreur 505
  });
});



//routes vers mes assets pour le bon fonctionnement du projet
app.get('/about', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/html/about.html'));
});

app.get('/contact', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/html/contact.html'));
});

app.get('/stylesheet.css', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/css/stylesheet.css'));
});

app.get('/stylesheetPopUp.css', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/css/stylesheetPopUp.css'));
});

app.get('/modifierTaches.js', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/js/modifierTaches.js'));
});

app.get('/favicon', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/images/logo.png'));
});

app.get('/todolist', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/images/todolist.png'));
});


// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
