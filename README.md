# ENSIBS-S2-Web-Projet

Projet s'inscrivant dans le module d'introduction au Web.
Ce module fait partie du cursus d'ingénieur Cyberdéfense de l'ENSIBS.
Cours présenté par Frédéric Linot.


### !
#### LA VIDEO DE DEMONSTRATION ETANT LOURDE, ELLE EST DISPONNIBLE EN TELECHARGEANT LES SOURCES VIA LE GIT !!###
#### > git clone git@gitlab.com:rom_ain/projet_webclient_cyberdef1.git
### !


# Installation et prérequis du projet - GET STARTED!

1) Télércharger les sources (Par SSH ou HTTPS): 
    >git clone git@gitlab.com:rom_ain/projet_webclient_cyberdef1.git
    >git clone https://gitlab.com/rom_ain/projet_webclient_cyberdef1.git


2) Télécharger les paquets NPM (prérequis: avoir déjà installé NPM sur sa machine)
    > cd ./todo-list/
    > npm install


3) Lancer la base de donnée (prérequis: avoir déjà installé Docker sur sa machine: voir ./BDD-Portable-Docker/readme.md)
    > cd ./BDD-Portable-Docker/
    > sudo docker-compose up


4) Lancer le serveur nodeJS
    > cd ./todo-list/
    > npm start
    
    //Le terminal doit afficher: 
            >[nodemon] starting `node index.js`
            >Server is running on port 3000.
            >Connexion à la base de données établie.


5) Afficher la page Web
    -> Rentrer l'url "http://localhost:3000/" sur le navigateur de son choix


**) Debug (quelques cas de figures qui nous sont arrivés lors du developpement)
    - si la base de donnée ne se lance pas avec la commande "docker-compose up", essayer en sudo comme décrit ci-dessus
    - si le lancement du serveur nodeJS renvoi une erreur de type "Error: listen EADDRINUSE: address already in use :::3000"
        - regarder les processus utilisant le port 3000
            > lsof -i:3000
        - kill le processus en question
            > kill -9 PID



# Structure du projet

./BDD-Portable-Docker/ -> contient la base de donnée sql ainsi que le fichier permettant de lancer docker
./todo-list/ -> contient les codes sources de l'application web

./todo-list/index.js -> API REST
./todo-list/db.js -> fichier de connexion à la BDD
./todo-list/test-db.js -> fichier de test de la BDD
./todo-list/package.json -> fihcier de configuration de nodeJS

./todo-list/views -> contient la partie cliente
./todo-list/views/index.ejs -> fichier racine de notre projet
./todo-list/views/html/ -> contient les fichiers html autre que l'index
./todo-list/views/css/ -> feuilles de styles
./todo-list/views/js -> contient les fonctions JS de notre proejt

