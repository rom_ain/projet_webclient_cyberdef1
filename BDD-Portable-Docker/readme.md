

# How to use this docker-BDD

You can change everything if you want !

## Prequisites
- Docker ( sous ubuntu : sudo apt install docker-ce )
- Docker-compose ( sous ubuntu : sudo apt install docker-compose )

## To start BDD :

	Docker-compose up


## To import todolist.sql file

Go to localhost:8080
Use the following credentials :

	user : root
	password : example

Let database field empty.

Click on import, choose your file and execute


## To connect, use this connection information on your db.js file :

	var config = {

		host     : 'localhost',

		port     : 3310,

		user     : 'root',

		password : 'example',

		database : 'todo_list'

	};

## To verify the following command on your project path :
node test-db.js
